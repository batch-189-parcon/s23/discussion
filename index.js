// console.log('yow')

/*
	OBJECTS
		An object is a data type that is used to represent real
		world objects. It is also a collection of related data
		and /or functionalities.

Creating objects using object literal
	Syntax:
		let objectName = {
				keyA: valueA,
				keyB: valueB

		}

*/

let cellphone = {
	name: "Nokia 3210",
	manufacturedDate: 1999
}

console.log("Result from creating objects")
console.log(cellphone)
console.log(typeof cellphone);

//Creating objects using a constructor function
/*
	Create a reusable function to create a several objects that 
	have the same data structure. This is useful for creating
	multiple instances/ copies of an object.

	Syntax:
		function ObjectName(keyA, keyB) {
			this.keyA = keyA;
			this.keyB = keyB
		}
*/

function Laptop (name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate
}

let laptop = new Laptop('Lenovo', 2008);
console.log("Result of creating objects using object constructors");
console.log(laptop);

let myLaptop = new Laptop("MacBook Air", [2020, 2021]);
console.log(myLaptop);

let oldLaptop = Laptop("Portal R2E CCMC, 1980");
console.log('Result from creating objects without the new keyword: ')
console.log(oldLaptop)

//Creating empty objects
let computer = {}
let myComputer = new Object();
console.log(computer);
console.log(myComputer);

//Accessing Object Property

//Using the dot notation
console.log("Result in dot notation: " + myLaptop.name)

//Using the square bracket notation
console.log("Result from square bracket notation: " + myLaptop["name"])

//Accessing array objects
let array = [laptop, myLaptop];
// let array = [{name: Lenovo, manufactureDate: 2008}, {name: MackBook Air, manufactureDate: 2020}]

//Square bracket notation
console.log(array[0]["name"])
//Dot notation
console.log(array[0].name);

//Initializing/Adding/Deleting/Reassigning Object Properties
let car = {}
console.log(car)

car.name = "Honda Civic";
console.log("Result from adding property using dot notation")
console.log(car)

car["manufacture date"] = 2019
console.log(car)

//Deleting object properties
delete car["manufacture date"];
console.log("Result from deleting object properties: ")
console.log(car)

//Reassigning object properties
car.name = "Tesla"
console.log("Result from reassigning property: ")
console.log(car)

//Object Methods
/*

	A method is a function which ia sa property of an object.
	They are also functions and one of the key differences they
	have is that methods are functions related to a specific object.

*/

let person = {
	name: "John",
	talk: function (){
		console.log("Hello! My name is " + this.name)
	}
}

console.log(person)
console.log("Result from object methods")
person.talk();

person.walk = function() {
		console.log(this.name + " walked 25 steps forward.")
}

person.walk()

let friend = {
	firstName: "Nehemiah",
	lastname: "Ellorico",
	address: {
		city: "Austin, Texas",
		country: "US"
	},
	emails: ["nejellorico@mail.com","nej123@mail.com"],
	introduce: function() {
		console.log("Hello! My name is " + this.firstName + " " + this.lastname);
	}
}
friend.introduce();

//Real World Application Objects
/*
	Scenario:
		1. We would like to create a game that would have several pokemon
			interact with each other.
		2. Every pokemon would have the same set of stats, properties and
			functions

*/

let myPokemon = {
	name: "Pikachu",
	level: "3",
	health: 100,
	attack: 50,
	tackle: function() {
		console.log("This pokemont tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	},
	faint: function() {
		console.log("Pokemon fainted.")
	}
}

console.log(myPokemon)

//Creating an object constructor
function Pokemon (name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	//Methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + " 's health is now reduced to " + (target.health - this.attack))
	}, 
	this.faint = function () {
		console.log(this.name + " fainted.")
	}
}

let pikachu = new Pokemon ("Pikachu", 16);
let squirtle = new Pokemon ("Squirtle", 8);

console.log(pikachu)
console.log(squirtle)

pikachu.tackle(squirtle)
pikachu.tackle(squirtle)